package com.company;

import java.util.Arrays;

public class NumbersAndStringProcessor {
    private String[] stringProcess;
    private int[] numbersProcess;

    public NumbersAndStringProcessor(String[] stringProcess, int[] numbersProcess) {
        this.stringProcess = stringProcess;
        this.numbersProcess = numbersProcess;
    }

    public String[] process(StringsProcess stringsProcessor){
        String[] resultArray = new String[stringProcess.length];
        for(int i = 0; i < stringProcess.length; i++){
            resultArray[i] = stringsProcessor.process(stringProcess[i]);
        }
        return resultArray;
    }

    public int[] process(NumbersProcess numbersProcessor){
        int[] resultArray = new int[numbersProcess.length];
        for(int i = 0; i < numbersProcess.length; i++){
            resultArray[i] = numbersProcessor.process(numbersProcess[i]);
        }
        return resultArray;
    }
}

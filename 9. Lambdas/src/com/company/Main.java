package com.company;

public class Main {

    public static void main(String[] args) {
        NumbersProcess numbersReverse = number -> {
	        int reversNumber  = 0;
	        while (number != 0){
                reversNumber = (reversNumber * 10) + number % 10;
                number /= 10;
            }
            return reversNumber;
	    };
        System.out.println("Revers Number: ");
        System.out.println(numbersReverse.process(12345));

        NumbersProcess removeNull = number -> {
            int numberWithOutNull = 0;
            int i = 1;
            while(number != 0){
                if ( number % 10 == 0){
                    number /= 10;
                }else{
                    numberWithOutNull = (int) (numberWithOutNull + ((number % 10) * i));
                    i *= 10;
                    number /= 10;
                }
            }
            return numberWithOutNull;
        };
        System.out.println("With Out Null: ");
        System.out.println(removeNull.process(1001110000));

        NumbersProcess oddNumber = number -> {
            int odd = 0;
            int i = 1;
            while (number != 0){
                if(number % 2 == 0){
                    odd = (int) (odd + ((number % 10) * i));
                }else{
                    odd = (int) (odd + ((number % 10 - 1) * i));
                }
                number /= 10;
                i *= 10;
            }
            return odd;
        };
        System.out.println("With out Odd Numbers: ");
        System.out.println(oddNumber.process(415613));

        StringsProcess getReverseString = process -> {
            StringBuilder builder = new StringBuilder(process);
            StringBuilder result = builder.reverse();
            return String.valueOf(result);
        };
        System.out.println("Revers to Strings: ");
        System.out.println(getReverseString.process("Hello"));

        StringsProcess getRemoveDigits = process -> {
          String newProcess = process.replaceAll("[0-9]","");
            return newProcess;
        };
        System.out.println("Remove digits from String: ");
        System.out.println(getRemoveDigits.process("R2D2"));

        StringsProcess getUpperString = process -> {
            String upper = process.toUpperCase();
            return upper;
        };
        System.out.println("Upper Case to String");
        System.out.println(getUpperString.process("upper"));

        String[] a = new String[2];
        a[0] = "abc";
        a[1] = "def";
        int[] b = new int[2];
        b[0] = 123;
        b[1] = 456;

        NumbersAndStringProcessor numbersAndStringProcessor = new NumbersAndStringProcessor(a, b);

        numbersAndStringProcessor.process(numbersReverse);
        numbersAndStringProcessor.process(getUpperString);

    }
}

import java.util.Arrays;
import java.util.Scanner;

class Program{
	public static void changeArrToNum(int array[]){
		int sum = 0;
		int j = array.length;
		for(int i = 0; i < array.length; i++){
			sum += array[i] * (Math.pow(10, j - 1));
			j--;
		}
		System.out.println(sum);
	}	

	public static void sortBubble(int array[]){
		for(int i = 0; i < array.length; i++){
			int position = array[i];
			for(int j = i - 1; j >= 0; j--){
				int left = array[j];
				if(position < left){
					array[j + 1] = left;
					array[j] = position;
				}else{
					break;
				}
			}
		}
		System.out.println(Arrays.toString(array));
	}
	
	public static void changeMinMax(int array[]){
		int min;
		int max;
		int positionMin = 0;
		int positionMax = 0;
		int temp = 0;

		for(int i = 0; i != array.length; i++){
			min = array[i];
			positionMin = i;
			for(int j = 0; j != array.length; j++){
				if(min > array[j]){
					min = array[j];
					positionMin = j;
				}
			}
		}
		for(int j = 0; j != array.length; j++){
			max = array[j];
			positionMax = j;
			for(int i = 0; i != array.length; i++){
				if(max < array[i]){
					max = array[i];
					positionMax = i;
				}
			}
		}
		temp = array[positionMax];
		array[positionMax] = array[positionMin];
		array[positionMin] = temp;

		System.out.println(Arrays.toString(array));
	}

	public static void getAverage(int array[]){
		int average = 0;
		for(int i = 0; i != array.length; i++){
			average = array[i] + average;
		}
		average /= array.length;
		System.out.println(average);
	}

	public static void getReverse(int array[]){
		int temp;

		for(int i = 0; i < array.length / 2; i++){
			temp = array[i]; //0 - 0
			array[i] = array[array.length - i - 1];
			array[array.length - i - 1] = temp; 
		}
		System.out.println(Arrays.toString(array));
		for(int i = 0; i < array.length / 2; i++){
			temp = array[i]; //0 - 0
			array[i] = array[array.length - i - 1];
			array[array.length - i - 1] = temp; 
		}
	}

	public static void getSumOfArray(int array[]){
		int sumOfArray = 0;

		for(int i = 0; i != array.length; i++){
			sumOfArray = array[i] + sumOfArray;
		}
		System.out.println(sumOfArray);
	}

	public static void main(String[] args) {
		System.out.println("Hello, this is program array's operation");
		System.out.println("Enter array size");
		Scanner scanner = new Scanner(System.in);
		int size = scanner.nextInt();
		int array[] = new int[size];
		System.out.println("Enter array's numbers");
		for(int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt();
		}
		System.out.println("Choose operation:");
		System.out.println("1. Show array");
		System.out.println("2. Array Sum");
		System.out.println("3. Revers array");
		System.out.println("4. Array average");
		System.out.println("5. Change minimal and maximal numbers");
		System.out.println("6. Realize bubble sort");
		System.out.println("7. Change from array to number");
		System.out.println("Any other number => Exit");
		int i = scanner.nextInt();
		while(i >= 1 && i <= 7){
			if(i == 1){
			System.out.println("______Array_______");
			System.out.println(Arrays.toString(array));
			System.out.println();
			}else if (i == 2){
				System.out.println("____Sum Of Array__");
				getSumOfArray(array);
				System.out.println();
			}else if(i == 3){
				System.out.println("_______Revers______");
				getReverse(array);
				System.out.println();
			}else if (i == 4){
				System.out.println("_______Average_____");
				getAverage(array);
				System.out.println();
			}else if (i == 5){
				System.out.println("_Change__Max_and_Min_");
				changeMinMax(array);
				System.out.println();
			}else if (i == 6){
				System.out.println("______Bubble_Sort_____");
				sortBubble(array);
				System.out.println();
			}else if(i == 7){
				System.out.println("__Change_array_to_number__");
				changeArrToNum(array);
			}
			System.out.println("Choose new operation: ");
			i = scanner.nextInt();
		}
		System.out.println("GoodBye");	
	}	
}
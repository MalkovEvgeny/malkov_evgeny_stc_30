import java.util.Scanner;

class Program1{

	public static int step(double n){
		if (n == 1){
			return 1;
		} else if(n > 1 && n < 2){
			return 0;
		}else{
			return step(n / 2);
		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);		
		double n = scanner.nextDouble();

		if(step(n) == 1){
			System.out.println("True");
		}else{
			System.out.println("False");
		}
	}	
}

public class Ellipse extends GeometricFigure {

    public Ellipse(double a, double b) {
        super(a, b);
    }

    @Override
    public double getArea() {
        area = PI * a * b;
        return area;
    }

    @Override
    public double getPerimeter() {
        perimeter = (((PI * a * b) + (Math.pow(a, 2)) - (2 * a * b) + Math.pow(b, 2)) / (a + b)) * 4;
        System.out.println(perimeter);
        return perimeter;
    }

}

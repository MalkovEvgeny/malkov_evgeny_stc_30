public interface Scale {

    void scale(double a, double b);
    void scaleRound(double r);
}

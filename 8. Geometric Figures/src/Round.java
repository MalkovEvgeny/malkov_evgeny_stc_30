public class Round extends GeometricFigure{
    public Round(double r) {
        super(r);
    }

    @Override
    public double getArea() {
        area = PI * Math.pow(r, 2);
        return area;
    }

    @Override
    public double getPerimeter() {
        perimeter = 2 * PI * r;
        System.out.println(perimeter);
        return perimeter;
    }

}

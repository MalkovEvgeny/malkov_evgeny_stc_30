public class Rectangle extends GeometricFigure {

    public Rectangle(double a, double b) {
        super(a, b);
    }

    @Override
    public double getArea() {
        area = a * b;
        return area;
    }

    @Override
    public double getPerimeter() {
        perimeter = (a * 2) + (b * 2);
        return perimeter;
    }
}

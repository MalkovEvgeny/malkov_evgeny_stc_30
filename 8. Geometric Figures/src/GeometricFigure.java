public abstract class GeometricFigure implements Scale, Moving {
    protected double a;
    protected double b;
    protected double r;
    protected double x;
    protected double y;

    protected static final double PI = Math.PI;

    protected double area;
    protected double perimeter;

    public GeometricFigure (double a, double b) {
        if(a > 0 && b > 0) {
            this.a = a;
            this.b = b;
        }else{
            System.exit(0);
        }
    }
    public GeometricFigure(double r) {
        this.r = r;
    }

    public abstract double getArea();

    public abstract double getPerimeter();

    public void moving(double dX, double dY) {
        x += dX;
        y += dY;
    }
    public void scale(double a, double b) {
        getArea();
        getPerimeter();
    }
    public void scaleRound(double r){
        getPerimeter();
        getArea();
    }



}

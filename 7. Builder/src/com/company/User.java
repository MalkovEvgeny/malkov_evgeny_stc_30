package com.company;

public class User {
    private String firstname;
    private String lastname;
    private int age;
    private boolean isWorker;

    public User (Builder builder) {
        this.firstname = builder.getFirstname();
        this.lastname = builder.getLastname();
        this.age = builder.getAge();
        this.isWorker = builder.isWorker();
    }
    public static Builder builder(){
        return new Builder();
    }
}

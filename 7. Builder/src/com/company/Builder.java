package com.company;

public class Builder {
    private String firstname;
    private String lastname;
    private int age;
    private boolean isWorker;

    public Builder firstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public Builder lastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public Builder age(int age) {
        this.age = age;
        return this;
    }

    public Builder isWorker(boolean isWorker) {
        this.isWorker = isWorker;
        return this;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public int getAge() {
        return age;
    }

    public boolean isWorker() {
        return isWorker;
    }

    public User build() {
        return new User(this);
    }
}
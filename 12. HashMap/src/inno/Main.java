package inno;

import inno.hashMap.HashMapImpl;

public class Main {

    public static void main(String[] args) {
        Map<String, String> map = new HashMapImpl<>();

        map.put("Евгений", "Малков");
        map.put("Марсель", "Сидиков");
        map.put("Евгений", "Малкин");
        map.put("Teheran", "Hello Teheran");
        map.put("Siblings", "Hello Siblings");

        System.out.println(map.get("Марсель"));
        System.out.println(map.get("Siblings"));
        System.out.println(map.get("Teheran"));
        System.out.println(map.get("ABC"));




    }
}

public class InnoArrayList implements InnoList {

    private static final int DEFAULT_SIZE = 10;
    private int elements[];
    private int count;

    public InnoArrayList() {
        this.elements = new int[DEFAULT_SIZE];
        this.count = 0;
    }

    @Override
    public void add(int element) {
        if (count == elements.length) {
            resize();
        }
        elements[count] = element;
        count++;
    }

    private void resize() {
        int newElements[] = new int[elements.length + elements.length / 2];

        for (int i = 0; i < elements.length; i++) {
            newElements[i] = elements[i];
        }
        this.elements = newElements;
    }

    @Override
    public void remove(int element) {
        int type = 0;
        for (int i = 0; i < count; i++){
            if(elements[i] == element) {
                elements[i] = elements[i + 1];
                elements[i + 1] = 0;
                type++;
            }
        }if (type != 0){
            count -= type;
        }
    }

    @Override
    public boolean contains(int element) {
        for (int i = 0; i < count; i++){
            if (elements[i] == element){
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            return -1;
        }
    }

    @Override
    public void insert(int index, int element) {
        if (index >= 0 && index < count){
            elements[index] = element;
        }
    }

    @Override
    public void addToBegin(int element) {
        if (count == elements.length) {
            resize();
        }
        for (int i = count; i >= 0; i--){
            elements[i + 1] = elements[i];
        }
        elements[0] = element;
        count++;
    }

    @Override
    public void removeByIndex(int index) {
        if (index >= 0 && index < count){
           for (int i = index; i < count; i++){
               elements[index] = elements[index + 1];
               index++;
           }
        }
        count--;
    }

    @Override
    public InnoIterator iterator() {
        return new InnoArrayListIterator();
    }

    private class InnoArrayListIterator implements InnoIterator {
        private int currentPosition;

        @Override
        public int next() {
            int nextValue = elements[currentPosition];
            currentPosition++;
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            return currentPosition < count;
        }
    }
}

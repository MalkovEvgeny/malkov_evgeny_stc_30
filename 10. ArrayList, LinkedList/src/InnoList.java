public interface InnoList extends InnoCollection {

    int get(int index);

    void insert(int index, int element);

    void addToBegin(int element);

    void removeByIndex(int index);
}

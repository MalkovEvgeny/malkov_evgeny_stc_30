public interface InnoIterator {

    int next();

    boolean hasNext();
}

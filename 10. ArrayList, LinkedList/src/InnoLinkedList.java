public class InnoLinkedList implements InnoList{

    private static class Node{
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    private Node first;
    private int count;
    private Node last;


    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null){
            first = newNode;
        }else{
            last.next = newNode;
        }
        last = newNode;
        count++;
    }

    @Override
    public void remove(int element) {
        Node current = first;
        for (int i = 0; i < count; i++){
            if (current.value == element){
                moveNodes(current);
                count--;
                break;
            }
            current = current.next;
        }
    }

    @Override
    public boolean contains(int element) {
        Node current = first;
        for (int i = 0; i < count; i++){
            if (current.value == element){
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        return new InnoLinkedListIterator();
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count){
            Node current = first;
            for (int i = 0; i < index; i++){
                current = current.next;
            }
            return current.value;
        }else{
            return -1;
        }
    }

    @Override
    public void insert(int index, int element) {
        Node current = first;
        if (index < count && index >=0) {
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            current.value = element;
        }
    }

    @Override
    public void addToBegin(int element) {
        Node newNode = new Node(element);
        newNode.next = first;
        first = newNode;
        count++;
    }

    @Override
    public void removeByIndex(int index) {
        Node current = first;
        if (index >= 0 && index < count) {
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            moveNodes(current);
            count--;
        }
    }

    private void moveNodes(Node current) {
        while(current.next.next != null){
                current.value = current.next.value;
                current = current.next;
        }
        current.value = current.next.value;
        current.next = null;
    }

    private class InnoLinkedListIterator implements InnoIterator{
        private Node current;

        InnoLinkedListIterator(){
            current = first;
        }

        @Override
        public int next() {
            int nextValue = current.value;
            current = current.next;
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            return current.next != null;
        }
    }
}

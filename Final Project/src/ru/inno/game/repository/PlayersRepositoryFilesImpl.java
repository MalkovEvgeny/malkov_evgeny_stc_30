package ru.inno.game.repository;

import ru.inno.game.models.Player;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;



public class PlayersRepositoryFilesImpl implements PlayersRepository {
    private String fileName;
    private String sequenceFileName;

    public PlayersRepositoryFilesImpl(String fileName, String sequenceFileName) {
        this.fileName = fileName;
        this.sequenceFileName = sequenceFileName;
    }

    @Override
    public Player findByNickname(String nickname) {
        try {
            List<Player> playerList = getPlayerListFromFile();
            for (Player player : playerList) {
                if (player.getName().equals(nickname)) {
                    return player;
                }
            }
                return null;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


    @Override
    public void save(Player player) {
        try {
            List<Player> playerList = getPlayerListFromFile();
            if (playerList.get(1).equals(player.getName())){
            }else {
                player.setId((generateId()));
                setPlayerInfoToFile(player);
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


    @Override
    public void update(Player player) {
        try {
            List<Player> playerList = getPlayerListFromFile();
            for (Player value : playerList) {
                if (value.getName().equals(player.getName())) {
                    value.setPoints(player.getPoints());
                }
            }
            setInfoToFile(playerList);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private void setInfoToFile(List<Player> playerList) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        StringBuilder sb = new StringBuilder();
        for (Player value : playerList) {
            sb.append(value.getIp()).append("#")
                    .append(value.getName()).append("#")
                    .append(value.getPoints()).append("#")
                    .append(value.getMaxWinsCount()).append("#")
                    .append(value.getMaxLosesCount()).append("#")
                    .append(value.getId()).append("\n");
        }
        writer.write(sb.toString());
        writer.close();
    }

    private Long generateId(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(sequenceFileName));
            String lastGenerateIdAsString = reader.readLine();
            long id = Long.parseLong(lastGenerateIdAsString);
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(sequenceFileName));
            writer.write(String.valueOf(id + 1));
            writer.close();

            return id;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private List<Player> getPlayerListFromFile() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        List<Player> playerList = reader
                .lines()
                .map(line -> {
                    String[] parsedLine = line.split("#");
                    return new Player(parsedLine[0], parsedLine[1], Integer.parseInt(parsedLine[2]),
                            Integer.parseInt(parsedLine[3]),Integer.parseInt(parsedLine[4]),
                            Long.parseLong(parsedLine[5]));
                })
                .collect(Collectors.toList());
        reader.close();
        return playerList;
    }

    private void setPlayerInfoToFile(Player player) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
        writer.write(player.getIp() + "#" + player.getName() + "#" + player.getPoints() + "#"
                + player.getMaxWinsCount() + "#" + player.getMaxLosesCount() + "#" + player.getId() + "\n");
        writer.close();
    }
}

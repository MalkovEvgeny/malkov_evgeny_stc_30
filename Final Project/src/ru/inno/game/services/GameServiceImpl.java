package ru.inno.game.services;

import ru.inno.game.dto.StatisticDto;
import ru.inno.game.models.Game;
import ru.inno.game.models.Player;
import ru.inno.game.models.Shot;
import ru.inno.game.repository.GamesRepository;
import ru.inno.game.repository.PlayersRepository;
import ru.inno.game.repository.ShotsRepository;

import java.time.LocalDateTime;

public class GameServiceImpl implements GameService {
    private PlayersRepository playersRepository;
    private GamesRepository gamesRepository;
    private ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        Player first = checkIfExists(firstIp, firstPlayerNickname);
        Player second = checkIfExists(secondIp, secondPlayerNickname);

        Game game = new Game(LocalDateTime.now(), first, second, 0, 0, 0L);
        gamesRepository.save(game);

        return game.getId();
    }

    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        if (player == null) {
            player = new Player(ip, nickname, 0, 0, 0);
            playersRepository.save(player);
        } else {
            player.setIp(ip);
            playersRepository.update(player);
        }
        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        Player shooter = playersRepository.findByNickname(shooterNickname);
        Player target = playersRepository.findByNickname(targetNickname);
        Game game = gamesRepository.findById(gameId);
        Shot shot = new Shot(LocalDateTime.now(), game, shooter, target);

        shooter.setPoints(shooter.getPoints() + 1);

        if (game.getPlayerFirst().getName().equals(shooterNickname)){
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        }
        if (game.getPlayerSecond().getName().equals(shooterNickname)){
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }

        playersRepository.update(shooter);
        gamesRepository.update(game);
        shotsRepository.save(shot);
    }
    @Override
    public StatisticDto finishGame(Long gameId){
        String first = gamesRepository.findById(gameId).getPlayerFirst().getName();
        String second = gamesRepository.findById(gameId).getPlayerSecond().getName();
        Integer firstShotsCount  = gamesRepository.findById(gameId).getPlayerFirstShotsCount();
        Integer secondShotsCount = gamesRepository.findById(gameId).getPlayerSecondShotsCount();
        Integer firstAllCount = playersRepository.findByNickname(first).getPoints();
        Integer secondAllCount = playersRepository.findByNickname(second).getPoints();
        LocalDateTime gameTime = gamesRepository.findById(gameId).getDateTime();
        String winner = getWinner(first, second, firstShotsCount, secondShotsCount);

        StatisticDto statisticDto = new StatisticDto(first, second, firstShotsCount, secondShotsCount, firstAllCount, secondAllCount, winner, gameId, gameTime);

        return statisticDto;
    }

    private String getWinner(String first, String second, Integer firstShotsCount, Integer secondShotsCount) {
        if (firstShotsCount > secondShotsCount) {
            return first;
        } else if (firstShotsCount < secondShotsCount){
            return second;
        } else {
            String result = "ничья";
            return result;
        }
    }
}

package ru.inno.game.services;

import ru.inno.game.dto.StatisticDto;


public interface GameService {
    Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname);

    void shot(Long gameId, String shooterNickname, String targetNickname);

    StatisticDto finishGame(Long gameId);
}

package ru.inno.game.dto;


import java.time.Duration;
import java.time.LocalDateTime;
import java.util.StringJoiner;

public class StatisticDto {
    private String first;
    private String second;
    private Integer firstShotsCount;
    private Integer secondShotsCount;
    private Integer firstAllCount;
    private Integer secondAllCount;
    private String winner;
    private Long gameId;
    private LocalDateTime gameTime;

    public StatisticDto(String first, String second, Integer firstShotsCount, Integer secondShotsCount, Integer firstAllCount, Integer secondAllCount, String winner, Long gameId, LocalDateTime gameTime) {
        this.first = first;
        this.second = second;
        this.firstShotsCount = firstShotsCount;
        this.secondShotsCount = secondShotsCount;
        this.firstAllCount = firstAllCount;
        this.secondAllCount = secondAllCount;
        this.winner = winner;
        this.gameId = gameId;
        this.gameTime = gameTime;
    }

    @Override
    public String toString() {
        return new StringJoiner("\n")
                .add("Игра с ID " + gameId)
                .add("Игрок 1: " + first + ", попаданий - " + firstShotsCount + ", всего очков - "
                        + firstAllCount)
                .add("Игрок 2: " + second + ", попаданий - " + secondShotsCount + ", всего очков - "
                        + secondAllCount)
                .add("Победа: " + winner)
                .add("Игра длилась: " + Duration.between(gameTime, LocalDateTime.now()).toMillis() / 1000 + " секунд")
                .toString();

    }
}

import java.util.Arrays;
class Program5{
	public static void main(String[] args) {
		
		int array[] = {9, 3, 5, -6, 2};

		System.out.println(Arrays.toString(array));

		for(int i = 0; i != array.length; i++){
			int position = array[i];
			
			for (int j = i - 1; j >= 0 ;j--) {
				int left = array[j];
				
				if (position < left) {
					array[j + 1] = left;
					array[j] = position;

				}else{
					
					break;
				}		
			}
		}
		System.out.println(Arrays.toString(array));
	}
}
import java.util.Scanner;
import java.util.Arrays;

class Program4{
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();
		int numbers[] = new int[size];

		int min = 0;
		int max = 0;
		int temp = 0;
		int positionofmin = 0;
		int positionofmax = 0;

		for (int i = 0; i != numbers.length; i++) {
			numbers[i] = sc.nextInt();		
		}	
		System.out.println(Arrays.toString(numbers));

		for (int i = 0; i != numbers.length; i++) {
			min = numbers[i];
			positionofmin = i;
			for(int j = 0; j != numbers.length; j++){
				if (numbers[j] < min){
					min = numbers[j];
					positionofmin = j;
				}
			}
		}
		for (int i = 0; i != numbers.length; i++) {
			max = numbers[i];
			positionofmax = i;
			for(int j = 0; j != numbers.length; j++){
				if (numbers[j] > max){
					max = numbers[j];
					positionofmax = j;
				}
			}
		}
		temp = numbers[positionofmin];
		numbers[positionofmin] = numbers[positionofmax];
		numbers[positionofmax] = temp;

		System.out.println(Arrays.toString(numbers));
	}
}
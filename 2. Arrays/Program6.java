class Program6 {
	public static void main(String args[]) {
		int array[] = {4, 2, 3, 5, 7};
		int number = 0;
		int j = array.length;
		
		for(int i = 0; i != array.length; i++){
			number += (array[i] * (Math.pow(10, j - 1))); 
			j--;
		}

		System.out.println(number); // программа должна вывести 42357
	}
}

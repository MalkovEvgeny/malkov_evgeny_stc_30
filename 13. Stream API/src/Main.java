
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Cars carsList = new Cars("Cars.txt");

        List<Integer> carsBlackOrNull = carsList.addToList().stream()
                .filter(cars -> cars.getColor().equals("Black") || cars.getKm() == 0)
                .map(Cars::getNumber)
                .collect(Collectors.toList());
        System.out.println(carsBlackOrNull);

        List<Integer> carPrice = carsList.addToList().stream()
                .filter(cars -> cars.getPrice() >= 700000 && cars.getPrice() <= 800000)
                .map(Cars::getPrice)
                .collect(Collectors.toList());
        System.out.println(carPrice.size());

        List<String> carColor = carsList.addToList().stream()
                .sorted(Comparator.comparingInt(Cars::getPrice))
                .map(Cars::getColor)
                .collect(Collectors.toList());
        System.out.println(carColor.get(0));


        List<Integer> camryPrice = carsList.addToList().stream()
                .filter(cars -> cars.getModel().contains("Camry"))
                .map(Cars::getPrice)
                .collect(Collectors.toList());

        int sum = 0;
        int count = 0;

        for (int i = 0; i < camryPrice.size(); i++){
            sum += camryPrice.get(i);
            count++;
        }
        int result = sum / count;
        System.out.println(result);


    }
}
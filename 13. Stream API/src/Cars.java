import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class Cars {
    private String fileName;
    private Integer number;
    private String model;
    private String color;
    private Integer km;
    private Integer price;

    public Cars(String fileName) {
        this.fileName = fileName;
    }

    public Cars(Integer number, String model, String color, Integer km, Integer price) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.km = km;
        this.price = price;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getKm() {
        return km;
    }

    public void setKm(Integer km) {
        this.km = km;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cars cars = (Cars) o;
        return Objects.equals(number, cars.number) && Objects.equals(model, cars.model) && Objects.equals(color, cars.color) && Objects.equals(km, cars.km) && Objects.equals(price, cars.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, model, color, km, price);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Cars.class.getSimpleName() + "[", "]")
                .add("number=" + number)
                .add("model='" + model + "'")
                .add("color='" + color + "'")
                .add("km=" + km)
                .add("price=" + price)
                .toString();
    }

    public List<Cars> addToList() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("Cars.txt"));
            List<Cars> cars = reader
                    .lines()
                    .map(line -> {
                        String parsedLine[] = line.split("#");
                        return new Cars(Integer.parseInt(parsedLine[0]), parsedLine[1], parsedLine[2],
                                Integer.parseInt(parsedLine[3]), Integer.parseInt(parsedLine[4]));
                    }).collect(Collectors.toList());
            return cars;
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }
}

package com.company;

public class Program {
    private String programName;

    public String getProgramName() {
        return programName;
    }

    public Program(String programName) {
        this.programName = programName;
    }

}
